<?php

namespace Threefold\WordPress\Core\OptionsPage;

use Carbon_Fields\Container;
use Carbon_Fields\Container\Theme_Options_Container;
use InvalidArgumentException;

/**
 * Trait CarbonFieldsOptionsPageTrait
 *
 * @package Threefold\WordPress\Core\OptionsPage
 */
trait CarbonFieldsOptionsPageTrait
{
    use OptionsPageTrait;

    /** @var Theme_Options_Container  */
    protected Theme_Options_Container $optionsPageContainer;

    /**
     * Returns config for post type options page
     *
     * @return array
     */
    abstract protected function getOptionsPageConfig() : array;

    /**
     * Register options page fields
     */
    abstract public function optionsPageFields() : void;

    /**
     * @param string $option
     *
     * @return string
     */
    abstract protected function getOptionKey(string $option = '') : string;

    /**
     * Define the options page with CMB2
     */
    public function registerOptionsPage() : void
    {
        // Get options page key
        $key = $this->getOptionsPageKey();
        if (empty($key)) {
            throw new InvalidArgumentException('`getOptionsPageKey` must return options page key string');
        }
        // Get options page config
        $config = $this->getOptionsPageConfig();
        if (empty($config['title'])) {
            throw new InvalidArgumentException('`getOptionsPageConfig` must return `title`');
        }
        // Create CMB instance for options page
        $this->optionsPageContainer = Container::make_theme_options($key, $config['title']);

        if (!empty($config['parentSlug'])) {
            $this->optionsPageContainer->set_page_parent($config['parentSlug']);
        }
        // Call function to handle adding fields to options page
        $this->optionsPageFields();
    }

    /**
     * Returns stored option using the option page key
     *
     * @param string $key
     * @param null $default
     *
     * @return mixed
     */
    public function _getOption(string $key, $default = null)
    {
        $optionKey = $this->getOptionKey($key);
        // Get option set key
        $option = carbon_get_theme_option($optionKey);
        // Return option set
        return $option ?? $default;
    }

}
