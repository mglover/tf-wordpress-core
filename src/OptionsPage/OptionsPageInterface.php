<?php

namespace Threefold\WordPress\Core\OptionsPage;

/**
 * Interface OptionsPageInterface
 *
 * @package Threefold\Theme\OptionsPage
 */
interface OptionsPageInterface
{
    /** @var string OPTIONS_PAGE_REGISTER_HOOK the action hook used to initialise the options page */
    const OPTIONS_PAGE_REGISTER_HOOK = 'admin_menu';

    /**
     * Register options page
     */
    public function registerOptionsPage() : void;

    /**
     * Returns an option value if not found returns default value
     *
     * @param string $key
     * @param null $default
     *
     * @return mixed|null
     */
    public function _getOption(string $key, $default = null);

}
