<?php

namespace Threefold\WordPress\Core\OptionsPage;

use CMB2;

/**
 * Interface CmbOptionsPageInterface
 *
 * @package Threefold\WordPress\Core\OptionsPage
 */
interface CmbOptionsPageInterface
{
    /** @var string OPTIONS_PAGE_REGISTER_HOOK the action hook used to initialise the options page */
    const OPTIONS_PAGE_REGISTER_HOOK = 'cmb2_admin_init';

    /**
     * Register options page
     */
    public function registerOptionsPage() : void;

    /**
     * Returns an option value if not found returns default value
     *
     * @param string $key
     * @param null $default
     *
     * @return mixed|null
     */
    public function _getOption(string $key, $default = null);

    /**
     * Register options page fields
     *
     * @param CMB2 $cmb
     */
    public function optionsPageFields(CMB2 $cmb) : void;

}
