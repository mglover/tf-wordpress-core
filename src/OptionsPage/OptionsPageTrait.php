<?php

namespace Threefold\WordPress\Core\OptionsPage;

/**
 * Trait OptionsPageTrait
 *
 * @method static mixed getOption(string $name, $default = null)
 *
 * @package Threefold\WordPress\Core\OptionsPage
 */
trait OptionsPageTrait
{
    /**
     * Returns options page key
     * Used for the option key and page url
     *
     * @return string
     */
    abstract protected function getOptionsPageKey() : string;

    /**
     * Define the options page
     */
    abstract public function registerOptionsPage() : void;

    /**
     * Returns stored option using the option page key
     *
     * @param string $key
     * @param null $default
     *
     * @return mixed
     */
    abstract public function _getOption(string $key, $default = null);

}
