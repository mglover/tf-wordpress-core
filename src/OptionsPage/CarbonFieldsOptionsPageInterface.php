<?php

namespace Threefold\WordPress\Core\OptionsPage;

use Carbon_Fields\Container\Theme_Options_Container;

/**
 * Interface CarbonFieldsOptionsPageInterface
 *
 * @package Threefold\WordPress\Core\OptionsPage
 */
interface CarbonFieldsOptionsPageInterface
{
    /** @var string OPTIONS_PAGE_REGISTER_HOOK the action hook used to initialise the options page */
    const OPTIONS_PAGE_REGISTER_HOOK = 'carbon_fields_register_fields';

    /**
     * Register options page
     */
    public function registerOptionsPage() : void;

    /**
     * Returns an option value if not found returns default value
     *
     * @param string $key
     * @param null $default
     *
     * @return mixed|null
     */
    public function _getOption(string $key, $default = null);

    /**
     * Register options page fields
     */
    public function optionsPageFields() : void;

}
