<?php

namespace Threefold\WordPress\Core\OptionsPage;

use CMB2;
use InvalidArgumentException;

/**
 * Trait CmbOptionsPageTrait
 *
 * @package Threefold\WordPress\Core\OptionsPage
 */
trait CmbOptionsPageTrait
{
    use OptionsPageTrait;

    /**
     * Returns config for post type options page
     *
     * @return array
     */
    abstract protected function getOptionsPageConfig() : array;

    /**
     * Register options page fields
     *
     * @param CMB2 $cmb
     */
    abstract public function optionsPageFields(CMB2 $cmb) : void;

    /**
     * Define the options page with CMB2
     */
    public function registerOptionsPage() : void
    {
        // Get options page key
        $key = $this->getOptionsPageKey();
        if (empty($key)) {
            throw new InvalidArgumentException('`getOptionsPageKey` must return options page key string');
        }
        // Get options page config
        $config = $this->getOptionsPageConfig();
        if (empty($config['title'])) {
            throw new InvalidArgumentException('`getOptionsPageConfig` must return `title`');
        }
        //
        $cmbConfig = [
            'id' => $key . '_metabox',
            'title' => $config['title'],
            'object_types' => ['options-page'],
            'option_key' => $key,
            'capability' => 'manage_options',
        ];

        if (!empty($config['parentSlug'])) {
            $cmbConfig['parent_slug'] = $config['parentSlug'];
        }
        // Create CMB instance for options page
        $cmb = new_cmb2_box($cmbConfig);
        // Call function to handle adding fields to options page
        $this->optionsPageFields($cmb);
    }

    /**
     * Returns stored option using the option page key
     *
     * @param string $key
     * @param null $default
     *
     * @return mixed
     */
    public function _getOption(string $key, $default = null)
    {
        // Get option set key
        $option = cmb2_get_option($this->getOptionsPageKey(), $key, $default);
        // Return option set
        return $option ?? $default;
    }

}
