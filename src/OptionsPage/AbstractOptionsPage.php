<?php

namespace Threefold\WordPress\Core\OptionsPage;

use Exception;
use Threefold\WordPress\Core\AdminNotice;
use Threefold\WordPress\Core\Loader\LoaderInterface;
use Threefold\WordPress\Core\StaticAccessor\StaticAccessorTrait;

/**
 * Class AbstractCmbOptionsPage
 *
 * @package Threefold\WordPress\Core\OptionsPage
 */
abstract class AbstractOptionsPage
{
    use StaticAccessorTrait;

    /**
     * AbstractCarbonFieldsOptionsPage constructor.
     *
     * @param LoaderInterface $loader
     */
    public function __construct(LoaderInterface $loader)
    {
        try {
            // Register options page
            $loader->addAction(static::OPTIONS_PAGE_REGISTER_HOOK ?? 'admin_menu', [$this, 'registerOptionsPage']);
        } catch (Exception $e) {
            AdminNotice::error($e->getMessage());
        }
    }

    /**
     * @param string $option
     *
     * @return string
     */
    protected function getOptionKey(string $option = '') : string
    {
        return $this->getOptionsPageKey() . '-' . $option;
    }
}
