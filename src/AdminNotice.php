<?php

namespace Threefold\WordPress\Core;

use Threefold\WordPress\Core\StaticAccessor\SingleStaticAccessorTrait;

/**
 * Class AdminNotice
 *
 * @method static void error(string $message) { @see AdminNotice::_error() }
 * @method static void warning(string $message) { @see AdminNotice::_warning() }
 * @method static void success(string $message) { @see AdminNotice::_success() }
 *
 * @package Threefold\WordPress\Core
 */
class AdminNotice
{
    use SingleStaticAccessorTrait;

    /** @var array $notices */
    protected array $notices = [];

    /**
     * AdminNotice constructor.
     */
    public function __construct()
    {
        Loader::addAction('all_admin_notices', [$this, 'processNotices']);
    }

    /**
     * Add message to error notices array
     *
     * @param string $message
     */
    public function _error(string $message) : void
    {
        $this->addNotice('error', $message);
    }

    /**
     * Add message to warning notices array
     *
     * @param string $message
     */
    public function _warning(string $message) : void
    {
        $this->addNotice('warning', $message);
    }

    /**
     * Add message to success notices array
     *
     * @param string $message
     */
    public function _success(string $message) : void
    {
        $this->addNotice('success', $message);
    }

    /**
     * Process any notices that have been set
     */
    public function processNotices() : void
    {
        // Process each notice type
        foreach ($this->notices as $type => $notices) {
            // Process each notice
            foreach ($notices as $notice) {
                $this->renderNotice($notice, $type);
            }
        }
    }

    /**
     * Add notice to render
     *
     * @param string $type
     * @param string $message
     */
    protected function addNotice(string $type, string $message) : void
    {
        if (!isset($this->notices[$type])) {
            $this->notices[$type] = [];
        }

        $this->notices[$type][] = $message;
    }

    /**
     * Print admin message
     *
     * @param string $notice
     * @param string $type
     */
    protected function renderNotice(string $notice, string $type = 'warning') : void
    {
        ?>
        <div id="message" class="notice notice-<?= $type ?>">
            <p><?= $notice ?></p>
        </div>
        <?php
    }
}
