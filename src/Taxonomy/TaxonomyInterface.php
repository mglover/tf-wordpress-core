<?php

namespace Threefold\WordPress\Core\Taxonomy;

interface TaxonomyInterface
{
    public static function instance(...$args);
}
