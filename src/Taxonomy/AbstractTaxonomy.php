<?php

namespace Threefold\WordPress\Core\Taxonomy;

use Exception;
use Threefold\WordPress\Core\AdminNotice;
use Threefold\WordPress\Core\Loader\LoaderInterface;
use Threefold\WordPress\Core\MetaData\TaxonomyMetaDataTrait;
use Threefold\WordPress\Core\StaticAccessor\StaticAccessorTrait;
use WP_Term;

/**
 * Class AbstractTaxonomy
 *
 * @method static WP_Term[] getAll(array $filter = [])
 * @method static WP_Term get(int $id)
 * @method static WP_Term getBySlug(string $slug)
 * @method static WP_Term[] getByPost(int $postIt)
 *
 * @package Threefold\WordPress\Core\Taxonomy
 */
class AbstractTaxonomy
{
    use StaticAccessorTrait;
    use LabelsTrait;
    use TaxonomyMetaDataTrait;

    /** @var string|null TAXONOMY name of the taxonomy */
    const TAXONOMY = null;
    /** @var string|null LABEL_SINGULAR string for singular labeling */
    const LABEL_SINGULAR = null;
    /** @var string|null LABEL_PLURAL string for plural labeling */
    const LABEL_PLURAL = null;

    /** @var array $postTypes */
    protected array $postTypes;

    /**
     * AbstractTaxonomy constructor.
     *
     * @param string|LoaderInterface $loader
     * @param array $postTypes
     */
    public function __construct(LoaderInterface $loader, array $postTypes = [])
    {
        try {
            // Check required definitions are defined
            if (empty(static::TAXONOMY) || empty(static::LABEL_SINGULAR) || empty(static::LABEL_PLURAL)) {
                throw new Exception(sprintf('The `%s` taxonomy requires the `TAXONOMY`, `LABEL_SINGULAR` and `LABEL_PLURAL` to be defined.',
                    static::class));
            }

            $this->postTypes = $postTypes;

            // Register taxonomy
            $loader->addAction('init', [$this, 'register'], 0);

            // Check if post fields method is defined
            if (method_exists($this, 'registerMetaFields')) {
                $postFieldsAction = static::META_FIELDS_REGISTER_HOOK ?? 'init';

                $loader->addAction($postFieldsAction, [$this, 'registerMetaFields']);
            }
        } catch (Exception $e) {
            AdminNotice::error($e->getMessage());
        }
    }

    /**
     * Register taxonomy
     */
    public function register() : void
    {
        register_taxonomy(static::TAXONOMY, $this->postTypes, $this->getDefinition());
    }

    /**
     * Returns taxonomy definition
     *
     * @return array
     */
    protected function getDefinition() : array
    {
        // Get label definition
        $labels = $this->getLabels();

        // Return taxonomy definition
        return [
            'hierarchical' => true,
            'labels' => $labels,
            'public' => true,
            'show_in_nav_menus' => true,
            'has_archive' => true,
            'query_var' => true,
            'show_ui' => true,
            'show_in_rest' => true,
            'rewrite' => [
                'slug' => static::TAXONOMY,
                'with_front' => true,
            ],
        ];
    }

    /**
     * @param array $filter
     *
     * @return array
     * @throws Exception
     */
    public function _getAll(array $filter = []) : array
    {
        $terms = get_terms(array_merge(['taxonomy' => static::TAXONOMY], $filter));

        if (is_wp_error($terms)) {
            throw new Exception($terms->get_error_message());
        }

        return $terms ?: [];
    }

    /**
     * @param int $id
     *
     * @return WP_Term
     * @throws Exception
     */
    public function _get(int $id) : ?WP_Term
    {
        $term = get_term($id, static::TAXONOMY);

        if (is_wp_error($term)) {
            throw new Exception($term->get_error_message());
        }

        return $term ?: null;
    }

    /**
     * @param string $slug
     *
     * @return WP_Term
     * @throws Exception
     */
    public function _getBySlug(string $slug) : ?WP_Term
    {
        $term = get_term_by('slug', $slug, static::TAXONOMY);

        if (is_wp_error($term)) {
            throw new Exception($term->get_error_message());
        }

        return $term ?: null;
    }

    /**
     * @param int $postId
     *
     * @return array
     * @throws Exception
     */
    public function _getByPost(int $postId) : array
    {
        $terms = get_the_terms($postId, static::TAXONOMY);

        if (is_wp_error($terms)) {
            throw new Exception($terms->get_error_message());
        }

        return $terms ?: [];
    }
}
