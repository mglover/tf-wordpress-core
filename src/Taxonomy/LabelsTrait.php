<?php

namespace Threefold\WordPress\Core\Taxonomy;

trait LabelsTrait
{
    /**
     * Returns array of taxonomy labels
     *
     * @return array
     */
    protected function getLabels() : array
    {
        $singular = static::LABEL_SINGULAR;
        $plural = static::LABEL_PLURAL;
        $labelPluralLowercase = strtolower($plural);

        return [
            'name' => __($plural),
            'singular_name' => __($singular),
            'search_items' => __('Search ' . $plural),
            'popular_items' => __('Popular ' . $plural),
            'all_items' => __('All ' . $plural),
            'parent_item' => __('Parent ' . $singular),
            'parent_item_colon' => __('Parent ' . $singular . ':'),
            'edit_item' => __('Edit ' . $singular),
            'view_item' => __('View ' . $singular),
            'update_item' => __('Update ' . $singular),
            'add_new_item' => __('Add New ' . $singular),
            'new_item_name' => __(sprintf('New %s Name', $singular)),
            'separate_items_with_commas' => __(sprintf('Separate %s with commas', $labelPluralLowercase)),
            'add_or_remove_items' => __('Add or remove ' . $labelPluralLowercase),
            'choose_from_most_used' => __('Choose from the most used ' . $labelPluralLowercase),
            'not_found' => __(sprintf('No %s found', $labelPluralLowercase)),
            'no_terms' => __('No ' . $labelPluralLowercase),
            'items_list_navigation' => __($plural . ' list navigation'),
            'items_list' => __($plural . ' list'),
            'most_used' => __('Most Used'),
            'back_to_items' => __('&larr; Back to ' . $plural),
        ];
    }

}
