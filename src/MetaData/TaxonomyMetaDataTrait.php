<?php

namespace Threefold\WordPress\Core\MetaData;

/**
 * Trait TaxonomyMetaDataTrait
 *
 * @method static mixed getMeta(int $postId, string $field, $default = false, bool $single = true)
 *
 * @package Threefold\WordPress\Core\MetaData
 */
trait TaxonomyMetaDataTrait
{
    /**
     * Returns term meta data with optional default value
     *
     * @param int $termId
     * @param string $field
     * @param mixed|null $default
     * @param bool $single
     *
     * @return mixed|null
     */
    public function _getMeta(int $termId, string $field, $default = null, bool $single = true)
    {
        $fieldKey = static::getMetaPrefix($field);

        switch (true) {
            case $this instanceof CarbonFieldsMetaDataInterface:
                $value = carbon_get_term_meta($termId, $fieldKey);
                break;

            default:
                $value = get_term_meta($termId, $fieldKey, $single);
                break;
        }

        return $value ?? $default;
    }

    /**
     * Returns string to be used as prefix for meta data field
     *
     * @param string $field
     *
     * @return string
     */
    public static function getMetaPrefix(string $field = '') : string
    {
        return static::TAXONOMY . '-' . $field;
    }
}
