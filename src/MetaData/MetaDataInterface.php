<?php

namespace Threefold\WordPress\Core\MetaData;

interface MetaDataInterface
{
    const META_FIELDS_REGISTER_HOOK = 'init';

    /**
     * Register meta data fields
     */
    public function registerMetaFields() : void;
}
