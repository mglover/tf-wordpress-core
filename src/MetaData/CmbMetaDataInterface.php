<?php

namespace Threefold\WordPress\Core\MetaData;

interface CmbMetaDataInterface
{
    const META_FIELDS_REGISTER_HOOK = 'cmb2_admin_init';

    /**
     * Register meta data fields
     */
    public function registerMetaFields() : void;
}
