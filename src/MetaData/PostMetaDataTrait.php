<?php

namespace Threefold\WordPress\Core\MetaData;

/**
 * Trait PostMetaDataTrait
 *
 * @method static mixed getMeta(int $postId, string $field, $default = null, bool $single = true)
 *
 * @package Threefold\WordPress\Core\MetaData
 */
trait PostMetaDataTrait
{
    /**
     * Returns post meta data with optional default value
     *
     * @param int $postId
     * @param string $field
     * @param mixed|null $default
     * @param bool $single
     *
     * @return mixed|null
     */
    public function _getMeta(int $postId, string $field, $default = null, bool $single = true)
    {
        $fieldKey = static::getMetaPrefix($field);

        switch (true) {
            case $this instanceof CarbonFieldsMetaDataInterface:
                $value = carbon_get_post_meta($postId, $fieldKey);
                break;

            default:
                $value = get_post_meta($postId, $fieldKey, $single);
                break;
    }

        return $value ?: $default;
    }

    /**
     * Returns string to be used as prefix for meta data field
     *
     * @param string $field
     *
     * @return string
     */
    public static function getMetaPrefix(string $field = '') : string
    {
        return static::POST_TYPE . '-' . $field;
    }

}
