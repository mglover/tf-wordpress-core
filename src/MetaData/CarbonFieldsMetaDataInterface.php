<?php

namespace Threefold\WordPress\Core\MetaData;

interface CarbonFieldsMetaDataInterface
{
    const META_FIELDS_REGISTER_HOOK = 'carbon_fields_register_fields';

    /**
     * Register meta data fields
     */
    public function registerMetaFields() : void;
}
