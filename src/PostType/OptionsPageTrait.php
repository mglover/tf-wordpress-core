<?php

namespace Threefold\WordPress\Core\PostType;

trait OptionsPageTrait
{
    /**
     * Returns post types options page key - used for the option key and page url
     * Only used if Options Page is implemented on a post type
     *
     * @return string
     */
    protected function getOptionsPageKey() : string
    {
        return static::POST_TYPE . '-options';
    }

    /**
     * Returns config for post type options page
     * Only used if Options Page is implemented on a post type
     *
     * @return array
     */
    protected function getOptionsPageConfig() : array
    {
        return [
            'title' => esc_html__(static::LABEL_SINGULAR . ' Options'),
            'parentSlug' => 'edit.php?post_type=' . static::POST_TYPE,
        ];
    }

    /**
     * @inheritDoc
     */
    protected function getOptionKey(string $option = '') : string
    {
        return static::POST_TYPE . '-' . $option;
    }

}
