<?php

namespace Threefold\WordPress\Core\PostType;

use Exception;
use Threefold\WordPress\Core\AdminNotice;
use Threefold\WordPress\Core\Loader\LoaderInterface;
use Threefold\WordPress\Core\MetaData\PostMetaDataTrait;
use Threefold\WordPress\Core\StaticAccessor\StaticAccessorTrait;
use WP_Query;

/**
 * Class AbstractPostType
 *
 * @method static WP_Post[] getAll(array $filter = [])
 *
 * @package Threefold\WordPress\Core\PostType
 */
abstract class AbstractPostType implements PostTypeInterface
{
    use StaticAccessorTrait;
    use PostMetaDataTrait;
    use LabelsTrait;
    use OptionsPageTrait;

    /** @var string|null POST_TYPE name of the post type */
    const POST_TYPE = null;
    /** @var string|null LABEL_SINGULAR string for singular labeling */
    const LABEL_SINGULAR = null;
    /** @var string|null LABEL_PLURAL string for plural labeling */
    const LABEL_PLURAL = null;
    /** @var string|null */
    const REWRITE_SLUG = null;

    /**
     * AbstractPostType constructor.
     *
     * @param LoaderInterface $loader
     */
    public function __construct(LoaderInterface $loader)
    {
        try {
            // Check required definitions are defined
            if (empty(static::POST_TYPE) || empty(static::LABEL_SINGULAR) || empty(static::LABEL_PLURAL)) {
                throw new Exception(sprintf('The `%s` post type requires the `POST_TYPE`, `LABEL_SINGULAR` and `LABEL_PLURAL` to be defined.',
                    static::class));
            }

            // Register post type
            $loader->addAction('init', [$this, 'register'], 0);
            // Check if options page method is defined
            if (defined('static::OPTIONS_PAGE_REGISTER_HOOK') && method_exists($this, 'registerOptionsPage')) {
                $loader->addAction(static::OPTIONS_PAGE_REGISTER_HOOK, [$this, 'registerOptionsPage']);
            }
            // Check if post fields method is defined
            if (defined('static::META_FIELDS_REGISTER_HOOK') && method_exists($this, 'registerMetaFields')) {
                $loader->addAction(static::META_FIELDS_REGISTER_HOOK, [$this, 'registerMetaFields']);
            }
        } catch (Exception $e) {
            AdminNotice::error($e->getMessage());
        }
    }

    /**
     * Register post type
     */
    public function register() : void
    {
        register_post_type(static::POST_TYPE, $this->getDefinition());
    }

    /**
     * Returns post type definition
     *
     * @return array
     */
    protected function getDefinition() : array
    {
        // Get label definition
        $labels = $this->getLabels();
        // Get supported fields definition
        $supports = $this->getSupports();
        // Get editor blocks template definition
        $template = $this->getTemplate();

        // Return post type definition
        return [
            'label' => __(static::POST_TYPE),
            'labels' => $labels,
            'public' => true,
            'menu_position' => 8,
            'has_archive' => true,
            'show_in_nav_menus' => true,
            'supports' => $supports,
            'show_in_rest' => true,
            'template' => $template,
            'rewrite' => [
                'slug' => static::REWRITE_SLUG === null ? static::POST_TYPE : static::REWRITE_SLUG,
            ]
        ];
    }

    /**
     * Returns array of post type supported fields
     *
     * @return array
     */
    protected function getSupports() : array
    {
        return [
            'title',
            'editor',
            'excerpt',
            'revisions',
            'thumbnail',
            'author',
            'custom-fields',
        ];
    }

    /**
     * Returns post type editor blocks template
     *
     * @return array
     */
    protected function getTemplate() : array
    {
        return [];
    }

    /**
     * @param array $filter
     *
     * @return array
     * @throws Exception
     */
    public function _getAll(array $filter = []) : array
    {
        $query = new WP_Query();

        $query->query(array_merge([
            'post_type' => static::POST_TYPE,
        ], $filter));

        $posts = $query->get_posts();

        if (is_wp_error($posts)) {
            throw new Exception($posts->get_error_message());
        }

        return $posts ?: [];
    }
}
