<?php

namespace Threefold\WordPress\Core\PostType;

/**
 * Trait PostTypeLabelsTrait
 *
 * @package Threefold\WordPress\Core\PostType
 */
trait LabelsTrait
{
    /**
     * Returns array of post type labels
     *
     * @return array
     */
    protected function getLabels() : array
    {
        $singular = static::LABEL_SINGULAR;;
        $plural = static::LABEL_PLURAL;
        $singularLowercase = strtolower($singular);
        $pluralLowercase = strtolower($plural);

        return [
            'name' => __($plural),
            'singular_name' => __($singular),
            'add_new' => __('Add New'),
            'add_new_item' => __('Add New ' . $singular),
            'edit_item' => __('Edit ' . $singular),
            'new_item' => __('New ' . $singular),
            'view_item' => __('View ' . $singular),
            'view_items' => __('View ' . $plural),
            'search_items' => __('Search ' . $plural),
            'not_found' => __(sprintf('No %s found.', $pluralLowercase)),
            'not_found_in_trash' => __(sprintf('No %s found in Trash.', $pluralLowercase)),
            'parent_item_colon' => __(sprintf('Parent %s:', $plural)),
            'all_items' => __('All ' . $plural),
            'archives' => __($singular . ' Archives'),
            'attributes' => __($singular . ' Attributes'),
            'insert_into_item' => __('Insert into ' . $singularLowercase),
            'uploaded_to_this_item' => __('Uploaded to this ' . $singularLowercase),
            'menu_name' => __($plural),
            'filter_items_list' => __(sprintf('Filter %s list', $pluralLowercase)),
            'items_list_navigation' => __($plural . ' list navigation'),
            'items_list' => __($plural . ' list'),
            'item_published' => __($singular . ' published'),
            'item_published_privately' => __($singular . ' published privately'),
            'item_reverted_to_draft' => __($singular . ' reverted to draft'),
            'item_scheduled' => __($singular . ' scheduled'),
            'item_updated' => __($singular . ' updated'),
        ];
    }
}
