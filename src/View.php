<?php

namespace Threefold\WordPress\Core;

use Threefold\WordPress\Core\StaticAccessor\LinkedStaticAccessorTrait;
use Threefold\WordPress\Core\View\{StaticViewInterface, View as ViewInstance, ViewInterface};

/**
 * Class View
 *
 * @method static string|null getPath(string $filePath, string $extension = 'php') { @see ViewInstance::getPath()
 * @method static string render(string $template, array $params = [], bool $output = true) { @see ViewInstance::render() }
 * @method static string renderPartial(string $template, string $partial, array $params = [], bool $output = true) { @see ViewInstance::renderPartial() }
 * @method static ViewInstance addDirectoryPath(string $path) { @see ViewInstance::addDirectoryPath() }
 * @method static ViewInstance addDirectoryPaths(array $paths = []) { @see ViewInstance::addDirectoryPaths() }
 * @method static ViewInstance removeDirectoryPath(string $path) { @see ViewInstance::removeDirectoryPath() }
 *
 * @package Threefold\WordPress\Core
 */
class View implements StaticViewInterface
{
    use LinkedStaticAccessorTrait;

    /**
     * @inheritDoc
     */
    public static function instance(...$args) : ViewInterface
    {
        return static::resolveInstance(...$args);
    }

    /**
     * @inheritDoc
     */
    protected static function makeInstance($args) : ViewInterface
    {
        return new ViewInstance(...$args);
    }

}
