<?php

namespace Threefold\WordPress\Core\View;

/**
 * Interface ViewInterface
 *
 * @package Threefold\WordPress\Core\View
 */
interface ViewInterface
{
    public function getPath(string $filePath, string $extension = 'php') : ?string;

    public function render(string $template, array $params = [], bool $output = true) : ?string;

    public function renderPartial(string $template, string $partial, array $params = [], bool $output = true) : ?string;

    public function addDirectoryPath(string $path, bool $priority = true) : self;

    public function addDirectoryPaths(array $paths = [], bool $priority = true) : self;

    public function removeDirectoryPath(string $path) : self;
}
