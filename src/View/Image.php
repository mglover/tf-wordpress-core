<?php

namespace Threefold\WordPress\Core\View;

use WP_Post;

/**
 * Class Image
 *
 * @package Threefold\WordPress\Core\View
 */
class Image
{
    private static array $svgCache = [];

    /**
     * @param int|WP_Post $attachment
     * @param string $size
     * @param array $attributes
     * @param bool $output
     *
     * @return string|null
     */
    public static function fromAttachment(
        $attachment,
        string $size = 'thumbnail',
        array $attributes = [],
        bool $output = true
    ) : ?string
    {
        if (!($attachment instanceof WP_Post)) {
            $image = wp_get_attachment_image_src($attachment, $size);
            $attachment = get_post($attachment);
        } else {
            $image = wp_get_attachment_image_src($attachment->ID, $size);
        }

        if (!$image) {
            return null;
        }
        [$src, $width, $height] = $image;

        $sizeClass = $size;

        if (is_array($sizeClass)) {
            $sizeClass = join('x', $sizeClass);
        }

        $attributes = wp_parse_args($attributes, [
            'src' => $src,
            'width' => $width ? intval($width) : null,
            'height' => $height ? intval($height) : null,
            'class' => "attachment-$sizeClass size-$sizeClass",
            'alt' => trim(strip_tags(get_post_meta($attachment->ID, '_wp_attachment_image_alt', true))),
            'loading' => 'lazy',
        ]);

        // Generate 'srcset' and 'sizes' if not already present.
        if (empty($attributes['srcset'])) {
            $imageMeta = wp_get_attachment_metadata($attachment->ID);

            if (is_array($imageMeta)) {
                $sizeArray = [absint($width), absint($height)];
                $srcset = wp_calculate_image_srcset($sizeArray, $src, $imageMeta, $attachment->ID);
                $sizes = wp_calculate_image_sizes($sizeArray, $src, $imageMeta, $attachment->ID);

                if ($srcset && ($sizes || !empty($attr['sizes']))) {
                    $attributes['srcset'] = $srcset;

                    if (empty($attr['sizes'])) {
                        $attributes['sizes'] = $sizes;
                    }
                }
            }
        }

        $attributes = apply_filters('wp_get_attachment_image_attributes', $attributes, $attachment, $size);

        $attributes = array_map('esc_attr', $attributes);
        $htmlAttributes = [];

        foreach ($attributes as $name => $value) {
            $htmlAttributes[] = sprintf('%s="%s"', $name, $value);
        }

        $html = '<img ' . implode(' ', $htmlAttributes) . '/>';

        if (!$output) {
            return $html;
        }

        echo $html;

        return null;
    }

    /**
     * @param $attachment
     * @param string $size
     * @param string $caption
     * @param array $attributes
     * @param bool $output
     *
     * @return string|null
     */
    public static function fromAttachmentWithCaption(
        $attachment,
        string $size = 'thumbnail',
        string $caption = '%s',
        array $attributes = [],
        bool $output = true
    ) : ?string
    {
        if (strpos($caption, '%s')) {
            if (!($attachment instanceof WP_Post)) {
                $attachment = get_post($attachment);
            }

            $caption = sprintf($caption, $attachment->post_excerpt);
        }

        $class = $attributes['class'] ?? '';
        $attributes['class'] = $attributes['imgClass'] ?? '';
        $captionClass = $attributes['captionClass'] ?? '';

        if (isset($attributes['imgClass'])) {
            unset($attributes['imgClass']);
        }
        if (isset($attributes['captionClass'])) {
            unset($attributes['captionClass']);
        }

        $html = '<figure class="' . $class . '">';
        $html .= static::fromAttachment($attachment, $size, $attributes, false);
        $html .= '<figcaption class="' . $captionClass. '">' . $caption . '</figcaption>';
        $html .= '</figure>';

        if (!$output) {
            return $html;
        }

        echo $html;

        return null;
    }

    /**
     * @param $post
     * @param string $size
     * @param array $attributes
     * @param bool $output
     *
     * @return string|null
     */
    public static function fromPost(
        $post,
        string $size = 'thumbnail',
        array $attributes = [],
        bool $output = true
    ) : ?string {
        if ($post instanceof WP_Post) {
            $attachment = get_post_thumbnail_id($post->ID);
        } else {
            $attachment = get_post_thumbnail_id($post);
        }

        return static::fromAttachment($attachment, $size, $attributes, $output);
    }

    /**
     * @param $post
     * @param string $size
     * @param string $caption
     * @param array $attributes
     * @param bool $output
     *
     * @return string|null
     */
    public static function fromPostWithCaption(
        $post,
        string $size = 'thumbnail',
        string $caption = '%s',
        array $attributes = [],
        bool $output = true
    ) : ?string
    {
        if ($post instanceof WP_Post) {
            $attachment = get_post_thumbnail_id($post->ID);
        } else {
            $attachment = get_post_thumbnail_id($post);
        }

        return static::fromAttachmentWithCaption($attachment, $size, $caption, $attributes, $output);
    }

    /**
     * Return SVG string from file content removing xml tag. Optionally can replace string in content for use to make ID attributes unique
     *
     * @param string $path
     * @param string|null $replace
     * @param string|null $with
     * @param bool $output
     *
     * @return string
     */
    public static function insertSvg(
        string $path,
        ?string $replace = null,
        ?string $with = null,
        bool $output = true
    ) : ?string {
        $cacheKey = md5($path);

        if (!isset(static::$svgCache[$cacheKey])) {
            if (!file_exists($path)) {
                static::$svgCache[$cacheKey] = sprintf('<pre>SVG not found `%s`</pre>', $path);
            } else {
                $file = file_get_contents($path);
                // Remove xml line
                $file = str_replace('<?xml version="1.0" encoding="UTF-8"?>', '', $file);
                static::$svgCache[$cacheKey] = $file;
            }
        }
        // Replace string if replacements provided
        if ($replace !== null && $with !== null) {
            $svg = str_replace($replace, $with, static::$svgCache[$cacheKey]);
        } else {
            $svg = static::$svgCache[$cacheKey];
        }

        if (!$output) {
            return $svg;
        }

        echo $svg;

        return null;
    }

    /**
     * @param int $userId
     * @param array $attributes
     * @param bool $output
     *
     * @return string|null
     */
    public static function avatar(int $userId, array $attributes = [], bool $output = true) : ?string
    {
        $author = get_userdata($userId);

        $attributes['class'] = $attributes['class'] ?? 'rounded-circle';

        $avatar = get_avatar(
            $userId,
            $attributes['size'] ?? 80,
            '',
            $attributes['size'] ?? $author->display_name,
            $attributes
        );

        if (!$output) {
            return $avatar;
        }

        echo $avatar;

        return null;
    }
}
