<?php

namespace Threefold\WordPress\Core\View;

/**
 * Interface StaticViewInterface
 *
 * @method static string|null getPath(string $filePath, string $extension = 'php') { @see View::getPath() }
 * @method static string|null render(string $template, array $params = [], bool $output = true) { @see View::render() }
 * @method static string|null renderPartial(string $template, string $partial, array $params = [], bool $output = true) { @see View::renderPartial() }
 * @method static View addDirectoryPath(string $path, bool $priority = true) { @see View::addTemplateDirPath() }
 * @method static View addDirectoryPaths(array $paths = [], bool $priority = true) { @see View::addTemplateDirPaths() }
 * @method static View removeDirectoryPath(string $path, bool $priority = true) { @see View::removeTemplateDirPath() }
 *
 * @package Threefold\WordPress\Core\View
 */
interface StaticViewInterface {}
