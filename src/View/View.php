<?php

namespace Threefold\WordPress\Core\View;

/**
 * Class View
 *
 * @package Threefold\WordPress\Core\View
 */
class View implements ViewInterface
{
    /** @var array */
    protected array $directoryPaths = [];
    /** @var array */
    protected array $pathCache = [];

    /**
     * View constructor.
     *
     * @param array $directoryPaths
     */
    public function __construct(array $directoryPaths = [])
    {
        // Add default directories
        $this->addDirectoryPaths($directoryPaths);
    }

    /**
     * Returns full path of specified file from partial details if it exists otherwise return null
     *
     * @param string $filePath
     * @param string $extension
     *
     * @return string|null
     */
    public function getPath(string $filePath, string $extension = 'php') : ?string
    {
        // Check file path is not empty
        if (empty($filePath)) {
            return null;
        }
        // Check if file path has extension and add if it doesn't
        if (empty(pathinfo($filePath, PATHINFO_EXTENSION))) {
            $filePath = sprintf('%s.%s', $filePath, $extension);
        }
        // Check if path already resolved
        $cacheKey = md5($filePath);
        if (!array_key_exists($cacheKey, $this->pathCache)) {
            $this->pathCache[$cacheKey] = null;
            // Check if file path is defined as a root path and check it exists
            if (substr($filePath, 0, 1) === '/' && file_exists($filePath)) {
                $this->pathCache[$cacheKey] = $filePath;
            } else {
                // Check directory paths for file
                foreach ($this->directoryPaths as $templateDirPath) {
                    $templatePath = sprintf('%s/%s', $templateDirPath, ltrim($filePath, '/'));

                    if (file_exists($templatePath)) {
                        $this->pathCache[$cacheKey] = $templatePath;
                        break;
                    }
                }
            }
        }
        // Return null - file not found
        return $this->pathCache[$cacheKey];
    }

    /**
     * Render content of template and either return or output the content
     *
     * @param string $template
     * @param array $params
     * @param bool $output
     *
     * @return string|null
     */
    public function render(string $template, array $params = [], bool $output = true) : ?string
    {
        // Get template path
        $templatePath = $this->getPath($template);
        // Check template found
        if (!$templatePath) {
            $content = sprintf('Template `%s` not found', $template);
        } else {
            // Extract parameters
            extract($params);
            // Capture render
            ob_start();
            require($templatePath);
            $content = ob_get_clean();
        }

        // If not output return content
        if (!$output) {
            return $content;
        }
        // Echo content and return
        echo $content;
        return null;
    }

    /**
     * Render content of template partial and either return or output the content
     *
     * @param string $template
     * @param string $partial
     * @param array $params
     * @param bool $output
     *
     * @return string|null
     */
    public function renderPartial(string $template, string $partial, $params = [], bool $output = true) : ?string
    {
        // Get partial template path
        if (!empty($partial)) {
            $partialTemplate = sprintf('%s-%s', $template, $partial);
            // Check partial template exists
            if ($this->getPath($partialTemplate)) {
                $template = $partialTemplate;
            }
        }
        // Return rendered template
        return $this->render($template, $params, $output);
    }

    /**
     * Add path to array of directories to search for template either first or last
     *
     * @param string $path
     * @param bool $priority
     *
     * @return View
     */
    public function addDirectoryPath(string $path, bool $priority = true) : self
    {
        if (!empty($path) && !in_array($path, $this->directoryPaths) && is_dir($path)) {
            if ($priority) {
                // Add path as first directory path
                array_unshift($this->directoryPaths, $path);
            } else {
                // Add path as last directory path
                $this->directoryPaths[] = $path;
            }
        }

        return $this;
    }

    /**
     * Add array of paths to array of directories either first or last
     *
     * @param array $paths
     * @param bool $priority
     *
     * @return View
     */
    public function addDirectoryPaths(array $paths = [], bool $priority = true) : self
    {
        // If priority reverse array to maintain defined order added
        if ($priority) {
            $paths = array_reverse($paths);
        }
        // Add paths to directory paths
        foreach ($paths as $path) {
            $this->addDirectoryPath($path, $priority);
        }

        return $this;
    }

    /**
     * Remove directory path from array of directories to search for template
     *
     * @param string $path
     *
     * @return View
     */
    public function removeDirectoryPath(string $path) : self
    {
        // Get array index of path to remove
        $index = array_search($path, $this->directoryPaths);
        // Remove path from array of directories if exists
        if ($index !== false) {
            unset($this->directoryPaths[$index]);
        }

        return $this;
    }

}
