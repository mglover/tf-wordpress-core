<?php

namespace Threefold\WordPress\Core;

use Threefold\WordPress\Core\StaticAccessor\StaticAccessorTrait;

/**
 * Class Core
 *
 * @method static string url(string $path)
 * @method static string path(string $path)
 *
 * @package Threefold\WordPress\Core
 */
abstract class AbstractCore
{
    use StaticAccessorTrait;

    /** @var string $rootUrl */
    protected string $rootUrl;
    /** @var string $rootPath */
    protected string $rootPath;
    /** @var array $mixManifest */
    private array $mixManifest;

    /**
     * Core constructor.
     *
     * @param string $rootUrl
     * @param string $rootPath
     */
    public function __construct(string $rootUrl, string $rootPath)
    {
        $this->rootUrl = rtrim($rootUrl, '/');
        $this->rootPath = rtrim($rootPath, '/');
    }

    /**
     * Returns full path to specified path
     *
     * @param string $path
     *
     * @return string
     */
    protected function _path(string $path = '') : string
    {
        return $this->rootPath . '/' . ltrim($path, '/');
    }

    /**
     * Returns the url after checking manifest.json for versioned url
     *
     * @param $urlPath
     *
     * @return string
     */
    protected function _url(string $urlPath) : string
    {
        if (empty($this->mixManifest)) {
            $manifestFile = $this->_path('mix-manifest.json');

            if (file_exists($manifestFile)) {
                // Extract mix manifest content
                $this->mixManifest = json_decode(file_get_contents($manifestFile), true);
            } else {
                // Set empty array when no mix manifest
                $this->mixManifest = [];
            }
        }

        $urlPathWithSlash = '/' . ltrim($urlPath, '/');
        // Return versioned URL from manifest
        if (array_key_exists($urlPathWithSlash, $this->mixManifest)) {
            return $this->rootUrl . $this->mixManifest[$urlPathWithSlash];
        }

        // No file was found in the manifest, return whatever was passed to mix().
        return $this->rootUrl . $urlPathWithSlash;
    }

}
