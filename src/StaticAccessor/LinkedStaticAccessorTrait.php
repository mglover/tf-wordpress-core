<?php

namespace Threefold\WordPress\Core\StaticAccessor;

/**
 * Trait LinkedStaticAccessorTrait
 *
 * @package Threefold\WordPress\Core\StaticAccessor
 */
trait LinkedStaticAccessorTrait
{
    use StaticAccessorTrait;

    /**
     * @inheritDoc
     */
    protected static function getStaticMethodName(string $method) : string
    {
        return $method;
    }
}
