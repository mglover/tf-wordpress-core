<?php

namespace Threefold\WordPress\Core\StaticAccessor;

use Exception;

/**
 * Trait StaticAccessorTrait
 *
 * @package Threefold\WordPress\Core\StaticAccessor
 */
trait StaticAccessorTrait
{
    /** @var mixed $instance */
    private static $instance;

    /**
     * Key used to identify an instance
     *
     * @return string
     */
    protected static function instanceIdentifier() : string
    {
        // Class used as default identifier
        return static::class;
    }

    /**
     * Return new instance
     *
     * @param array $args
     *
     * @return static
     */
    protected static function makeInstance($args = [])
    {
        return new static(...$args);
    }

    /**
     * Return existing instance or create a new instance
     *
     * @param array $args
     *
     * @return mixed
     */
    protected static function resolveInstance($args = [])
    {
        // Get instance identifier
        $identifier = static::instanceIdentifier();
        // Check if identifier instance is defined
        if (empty(static::$instance[$identifier])) {
            // Create new identifier instance
            static::$instance[$identifier] = static::makeInstance($args);
        }

        // Return identifier instance
        return static::$instance[$identifier];
    }

    /**
     * Returns an instance
     *
     * @param $args
     *
     * @return mixed
     */
    public static function instance(...$args)
    {
        return static::resolveInstance($args);
    }

    /**
     * Convert static method name to instance method name
     *
     * @param string $method
     *
     * @return string
     */
    protected static function getStaticMethodName(string $method) : string
    {
        return '_' . $method;
    }

    /**
     * Magic method to handle calling instance methods statically
     *
     * @param $method
     * @param $args
     *
     * @return mixed
     * @throws Exception
     */
    public static function __callStatic($method, $args)
    {
        // Get class instance
        $instance = static::instance();
        // Create instance method name from statically called method name
        $methodName = static::getStaticMethodName($method);
        // Check method exists on instance
        if (!method_exists($instance, $methodName)) {
            throw new Exception(sprintf('Invalid static method `%s` on `%s`', $methodName, static::class));
        }

        // Return response from instance method
        return $instance->$methodName(...$args);
    }
}
