<?php

namespace Threefold\WordPress\Core\StaticAccessor;

/**
 * Trait SingleStaticAccessorTrait
 *
 * @package Threefold\WordPress\Core\StaticAccessor
 */
trait SingleStaticAccessorTrait
{
    use StaticAccessorTrait;

    /**
     * @inheritDoc
     */
    protected static function resolveInstance($args = [])
    {
        // Check if single instance is defined
        if (empty(static::$instance)) {
            // Create new instance
            static::$instance = static::makeInstance($args);
        }

        // Return instance
        return static::$instance;
    }

}
