<?php

namespace Threefold\WordPress\Core\Loader;

use Exception;
use Threefold\WordPress\Core\StaticAccessor\StaticAccessorTrait;

/**
 * Class Loader
 *
 * @package Threefold\WordPress\Core\Loader
 */
class Loader implements LoaderInterface
{
    use StaticAccessorTrait;

    /** @var string|callable */
    private $baseUrl;
    /** @var bool */
    private bool $enqueueEnabled;
    /** @var bool */
    private bool $hasLoaded = false;
    /** @var array */
    private array $actions = [];
    /** @var array */
    private array $filters = [];
    /** @var array */
    private array $scripts = [
        'wp' => [],
        'admin' => [],
        'block' => [],
        'block-admin' => [],
    ];
    /** @var array $styles */
    private array $styles = [
        'wp' => [],
        'admin' => [],
        'block' => [],
        'block-admin' => [],
    ];

    /**
     * Loader constructor.
     */
    public function __construct()
    {
        $this->enqueueEnabled = false;
    }

    /**
     * Override the base url used for scripts and styles
     *
     * @param string|callable $url
     *
     * @return $this
     */
    public function setBaseUrl($url) : self
    {
        $this->baseUrl = $url;
        $this->enableEnqueue();

        return $this;
    }

    /**
     * Enable enqueuing of scripts and styles
     *
     * @return $this
     */
    public function enableEnqueue() : self
    {
        $this->enqueueEnabled = true;

        return $this;
    }

    /**
     * Disable enqueuing of scripts and styles
     *
     * @return $this
     */
    public function disableEnqueue() : self
    {
        $this->enqueueEnabled = false;

        return $this;
    }

    /**
     * Add a new action to the collection to be registered with WordPress.
     *
     * @param string $hook
     * @param callable $callback
     * @param int $priority
     * @param int $arguments
     *
     * @return Loader
     */
    public function addAction(string $hook, callable $callback, int $priority = 10, int $arguments = 1) : self
    {
        $this->addHook('actions', $hook, $callback, $priority, $arguments);

        return $this;
    }

    /**
     * Add a new filter to the collection to be registered with WordPress.
     *
     * @param string $hook
     * @param callable $callback
     * @param int $priority
     * @param int $arguments
     *
     * @return Loader
     */
    public function addFilter(string $hook, callable $callback, int $priority = 10, int $arguments = 1) : self
    {
        $this->addHook('filters', $hook, $callback, $priority, $arguments);

        return $this;
    }

    /**
     * Add a new script to the collection to be registered with WordPress.
     *
     * @param string $handle
     * @param string $path
     * @param array $dependencies
     * @param array $config
     *
     * @return Loader
     * @throws Exception
     */
    public function addScript(string $handle, string $path, array $dependencies = [], array $config = []) : self
    {
        $type = ($config['isAdmin'] ?? false) ? 'admin' : 'wp';
        $this->addScriptHandle($type, $handle, $path, $dependencies, $config);

        return $this;
    }

    /**
     * Add a new block script to the collection to be registered with WordPress.
     *
     * @param string $handle
     * @param string $path
     * @param array $dependencies
     * @param array $config
     *
     * @return Loader
     * @throws Exception
     */
    public function addBlockScript(
        string $handle,
        string $path,
        array $dependencies = [],
        array $config = []
    ) : self {
        $config['autoload'] ??= false;

        $type = ($config['isAdmin'] ?? false) ? 'block-admin' : 'block';
        $this->addScriptHandle($type, $handle, $path, $dependencies, $config);

        return $this;
    }

    /**
     * Add localize data to existing script handle
     *
     * @param string $handle
     * @param array $data
     * @param array $config
     *
     * @return $this
     * @throws Exception
     */
    public function addScriptData(string $handle, array $data, array $config = []) : self
    {
        $type = ($config['isAdmin'] ?? false) ? 'admin' : 'wp';
        $this->addScriptHandleData($type, $handle, $data, $config);

        return $this;
    }

    /**
     * Add localize data to existing block script handle
     *
     * @param string $handle
     * @param array $data
     * @param array $config
     *
     * @return Loader
     * @throws Exception
     */
    public function addBlockScriptData(string $handle, array $data, array $config = []) : self
    {
        $type = ($config['isAdmin'] ?? false) ? 'block-admin' : 'block';
        $this->addScriptHandleData($type, $handle, $data, $config);

        return $this;
    }

    /**
     * Add a new style to the collection to be registered with WordPress.
     *
     * @param string $handle
     * @param string $path
     * @param array $dependencies
     * @param array $config
     *
     * @return Loader
     * @throws Exception
     */
    public function addStyle(string $handle, string $path, array $dependencies = [], array $config = []) : self
    {
        $type = ($config['isAdmin'] ?? false) ? 'admin' : 'wp';
        $this->addStyleHandle($type, $handle, $path, $dependencies, $config);

        return $this;
    }

    /**
     * Add a new block style to the collection to be registered with WordPress.
     *
     * @param string $handle
     * @param string $path
     * @param array $dependencies
     * @param array $config
     *
     * @return Loader
     * @throws Exception
     */
    public function addBlockStyle(string $handle, string $path, array $dependencies = [], array $config = []) : self
    {
        $config['autoload'] ??= false;

        $type = ($config['isAdmin'] ?? true) ? 'block-admin' : 'block';
        $this->addStyleHandle($type, $handle, $path, $dependencies, $config);

        return $this;
    }

    /**
     * Register the filters and actions with WordPress
     */
    public function load() : void
    {
        if ($this->enqueueEnabled) {
            // Add enqueue script hooks
            $this->addAction('wp_enqueue_scripts', [$this, 'loadWpScripts']);
            $this->addAction('admin_enqueue_scripts', [$this, 'loadAdminScripts']);
            $this->addAction('enqueue_block_assets', [$this, 'loadBlockScripts'], 1, 0);
            $this->addAction('enqueue_block_editor_assets', [$this, 'loadBlockAdminScripts'], 1, 0);
            // Add enqueue style hooks
            $this->addAction('wp_enqueue_scripts', [$this, 'loadWpStyles']);
            $this->addAction('admin_enqueue_scripts', [$this, 'loadAdminStyles']);
            $this->addAction('enqueue_block_assets', [$this, 'loadBlockStyles'], 1, 0);
            $this->addAction('enqueue_block_editor_assets', [$this, 'loadBlockAdminStyles'], 1, 0);
        }
        // Register actions
        $this->registerActions($this->actions);
        // Register filters
        $this->registerFilters($this->filters);

        $this->hasLoaded = true;
    }

    /**
     * Process front end scripts
     */
    public function loadWpScripts() : void
    {
        $loadHandles = $this->registerScripts($this->scripts['wp']);
        $this->loadScripts($loadHandles);
    }

    /**
     * Process admin scripts
     */
    public function loadAdminScripts() : void
    {
        $loadHandles = $this->registerScripts($this->scripts['admin']);
        $this->loadScripts($loadHandles);
    }

    /**
     * Process block scripts
     */
    public function loadBlockScripts() : void
    {
        $loadHandles = $this->registerScripts($this->scripts['block']);
        $this->loadScripts($loadHandles);
    }

    /**
     * Process block scripts
     */
    public function loadBlockAdminScripts() : void
    {
        $loadHandles = $this->registerScripts($this->scripts['block-admin']);
        $this->loadScripts($loadHandles);
    }

    /**
     * Process front end styles
     */
    public function loadWpStyles() : void
    {
        $loadHandles = $this->registerStyles($this->styles['wp']);
        $this->loadStyles($loadHandles);
    }

    /**
     * Process admin styles
     */
    public function loadAdminStyles() : void
    {
        $loadHandles = $this->registerStyles($this->styles['admin']);
        $this->loadStyles($loadHandles);
    }

    /**
     * Process block styles
     */
    public function loadBlockStyles() : void
    {
        $loadHandles = $this->registerStyles($this->styles['block']);
        $this->loadStyles($loadHandles);
    }

    /**
     * Process block styles
     */
    public function loadBlockAdminStyles() : void
    {
        $loadHandles = $this->registerStyles($this->styles['block-admin']);
        $this->loadStyles($loadHandles);
    }

    /**
     * A utility function that is used to register the actions and hooks into a single collection
     *
     * @param string $type
     * @param string $hook
     * @param callable $callback
     * @param int $priority
     * @param int $arguments
     */
    private function addHook(string $type, string $hook, callable $callback, int $priority, int $arguments) : void
    {
        $hookConfig = [
            'hook' => $hook,
            'callback' => $callback,
            'priority' => $priority,
            'arguments' => $arguments,
        ];

        $this->{$type}[] = $hookConfig;

        if ($this->hasLoaded) {
            $registerMethod = 'register' . ucfirst($type);
            $this->{$registerMethod}([$hookConfig]);
        }
    }

    /**
     * A utility function that is used to register the scripts into their collections
     *
     * @param string $type
     * @param string $handle
     * @param string $path
     * @param array $dependencies
     * @param array $config
     *
     * @throws Exception
     */
    private function addScriptHandle(
        string $type,
        string $handle,
        string $path,
        array $dependencies,
        array $config
    ) : void {
        if (isset($this->scripts[$type][$handle])) {
            if ($this->scripts[$type][$handle]['path'] === $path) {
                return;
            }

            throw new Exception(sprintf('Script handle `%s` already exists', $handle));
        }

        $this->scripts[$type][$handle] = [
            'path' => $path,
            'dependencies' => $dependencies,
            'version' => $config['version'] ?? null,
            'inFooter' => $config['inFooter'] ?? true,
            'autoload' => $config['autoload'] ?? true,
            'data' => $config['data'] ?? [],
            'dataKey' => $config['dataKey'] ?? null,
        ];
    }

    /**
     * A utility function that is used to add localize data to the registered scripts
     *
     * @param string $type
     * @param string $handle
     * @param array $data
     * @param array $options
     *
     * @throws Exception
     */
    private function addScriptHandleData(string $type, string $handle, array $data, array $options) : void
    {
        // Check handle exists
        if (!isset($this->scripts[$type][$handle])) {
            throw new Exception(sprintf('Script handle `%s` does not exist', $handle));
        }
        // Shorthand definition
        $script = $this->scripts[$type][$handle];
        // If defining the localizeKey, check has not already been defined or localized data already defined with default key expected
        if (!empty($options['key'])) {
            if (!empty($script['data']) && $script['dataKey'] !== $options['key']) {
                throw new Exception(sprintf(
                    '`data` already defined and `dataKey` does not match already defined `dataKey` (%s)',
                    $script['dataKey']
                ));
            } else {
                $script['dataKey'] = $options['key'];
            }
        }
        // Merge localize data
        $script['data'] = $this->mergeData($script['data'], $data);

        $this->scripts[$type][$handle] = $script;
    }

    private function mergeData($data, $newData)
    {
        if (!is_array($newData)) {
            return $newData;
        }

        if (empty($data)) {
            return $newData;
        }

        if (array_keys($newData) === range(0, count($newData) - 1)) {
            return array_merge($data, $newData);
        }

        foreach ($newData as $key => $value) {
            $data[$key] = $this->mergeData($data[$key] ?? null, $value);
        }

        return $data;
    }

    /**
     * A utility function that is used to register the styles into their collections
     *
     * @param string $type
     * @param string $handle
     * @param string $path
     * @param array $dependencies
     * @param array $config
     *
     * @throws Exception
     */
    private function addStyleHandle(
        string $type,
        string $handle,
        string $path,
        array $dependencies,
        array $config
    ) : void {
        if (isset($this->styles[$type][$handle])) {
            throw new Exception(sprintf('Style handle `%s` already exists', $handle));
        }

        $this->styles[$type][$handle] = [
            'path' => $path,
            'dependencies' => $dependencies,
            'version' => $config['version'] ?? null,
            'media' => $config['media'] ?? 'all',
            'autoload' => $config['autoload'] ?? true,
        ];
    }

    /**
     * Register actions with WordPress
     *
     * @param array $actions
     */
    private function registerActions(array $actions) : void
    {
        foreach ($actions as $hook) {
            add_action($hook['hook'], $hook['callback'], $hook['priority'], $hook['arguments']);
        }
    }

    /**
     * Register filters with WordPress
     *
     * @param array $filters
     */
    private function registerFilters(array $filters) : void
    {
        foreach ($filters as $hook) {
            add_filter($hook['hook'], $hook['callback'], $hook['priority'], $hook['arguments']);
        }
    }

    /**
     * Register scripts with WordPress
     *
     * @param array $scripts
     *
     * @return array
     */
    private function registerScripts(array $scripts) : array
    {
        $loadHandles = [];

        foreach ($scripts as $handle => $script) {
            [
                'path' => $path,
                'dependencies' => $dependencies,
                'version' => $version,
                'inFooter' => $inFooter,
                'autoload' => $autoload,
                'data' => $data,
                'dataKey' => $dataKey,
            ] = $script;
            // Process script URL
            $path = $this->processAssetUrl($path);
            // Register handle
            wp_register_script($handle, $path, $dependencies, $version, $inFooter);
            // Check if script is autoload
            if ($autoload) {
                $loadHandles[] = $handle;
            }
            // Assign any localized data to handle
            if (!empty($data)) {
                if (empty($dataKey)) {
                    $dataKey = $handle . 'Config';
                }

                // Convert data key from kebab case to pascal case for consistant use in javascript
                $dataKey = ucwords($dataKey, '-_');
                $dataKey = lcfirst(str_replace(['-', '_'], '', $dataKey));

                wp_localize_script($handle, $dataKey, $data);
            }
        }

        return $loadHandles;
    }

    /**
     * Enqueue scripts with WordPress
     *
     * @param array $handles
     */
    private function loadScripts(array $handles) : void
    {
        foreach ($handles as $handle) {
            // Enqueue handle
            wp_enqueue_script($handle);
        }
    }

    /**
     * Register styles with WordPress
     *
     * @param array $styles
     *
     * @return array
     */
    private function registerStyles(array $styles) : array
    {
        $loadHandles = [];

        foreach ($styles as $handle => $style) {
            [
                'path' => $path,
                'dependencies' => $dependencies,
                'version' => $version,
                'media' => $media,
                'autoload' => $autoload,
            ] = $style;
            // Process style URL
            $path = $this->processAssetUrl($path);
            // Register handle
            wp_register_style($handle, $path, $dependencies, $version, $media);
            // Check if handle should autoload
            if ($autoload) {
                $loadHandles[] = $handle;
            }
        }

        return $loadHandles;
    }

    /**
     * Enqueue styles with WordPress
     *
     * @param array $handles
     */
    private function loadStyles(array $handles) : void
    {
        foreach ($handles as $handle) {
            // Enqueue handle
            wp_enqueue_style($handle);
        }
    }

    /**
     * Return full path URL
     *
     * @param string $path
     *
     * @return mixed|string
     */
    private function processAssetUrl(string $path) : string
    {
        // If defined as root url `/...` or `http...` return path
        if (substr($path, 0, 1) === '/' || substr($path, 0, 4) === 'http') {
            return $path;
        }
        // If loader base url is callable process path
        if (is_callable($this->baseUrl)) {
            return call_user_func($this->baseUrl, $path);
        }

        // Return path appended to base url
        return $this->baseUrl . '/' . $path;
    }
}
