<?php

namespace Threefold\WordPress\Core\Loader;

/**
 * Interface LoaderInterface
 *
 * @package Threefold\WordPress\Core\Loader
 */
interface LoaderInterface
{
    public function setBaseUrl($url) : self;

    public function enableEnqueue() : self;

    public function disableEnqueue() : self;

    public function addAction(string $hook, callable $callback, int $priority = 10, int $arguments = 1) : self;

    public function addFilter(string $hook, callable $callback, int $priority = 10, int $arguments = 1) : self;

    public function addScript(string $handle, string $path, array $dependencies = [], array $config = []) : self;

    public function addScriptData(string $handle, array $data, array $options = []) : self;

    public function addStyle(string $handle, string $path, array $dependencies = [], array $config = []) : self;

    public function addBlockScript(string $handle, string $path, array $dependencies = [], array $config = []) : self;

    public function addBlockScriptData(string $handle, array $data, array $options = []) : self;

    public function addBlockStyle(string $handle, string $path, array $dependencies = [], array $config = []) : self;

    public function load() : void;
}
