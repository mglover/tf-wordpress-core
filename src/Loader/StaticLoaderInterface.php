<?php

namespace Threefold\WordPress\Core\Loader;

/**
 * Interface StaticLoaderInterface
 *
 * @method static Loader addAction(string $hook, callable $callback, int $priority = 10, int $arguments = 1) { @see Loader::addAction() }
 * @method static Loader addFilter(string $hook, callable $callback, int $priority = 10, int $arguments = 1) { @see Loader::addFilter() }
 * @method static Loader addScript(string $handle, string $path, array $dependencies = [], array $config = []) { @see Loader::addScript() }
 * @method static Loader addScriptData(string $handle, array $data = [], string $key = null) { @see Loader::addScriptData() }
 * @method static Loader addStyle(string $handle, string $path, array $dependencies = [], array $config = []) { @see Loader::addStyle() }
 * @method static Loader addBlockScript(string $handle, string $path, array $dependencies = [], array $config = []) { @see Loader::addBlockScript() }
 * @method static Loader addBlockScriptData(string $handle, array $data = [], string $key = null) { @see Loader::addBlockScriptData() }
 * @method static Loader addBlockStyle(string $handle, string $path, array $dependencies = [], array $config = []) { @see Loader::addBlockStyle() }
 * @method static void load() { @see Loader::_load() }
 *
 * @package Threefold\WordPress\Core\Loader
 */
interface StaticLoaderInterface {}
