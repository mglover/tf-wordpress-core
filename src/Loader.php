<?php

namespace Threefold\WordPress\Core;

use Threefold\WordPress\Core\Loader\{Loader as LoaderInstance, LoaderInterface, StaticLoaderInterface};
use Threefold\WordPress\Core\StaticAccessor\LinkedStaticAccessorTrait;

/**
 * Class Loader
 *
 * @method static LoaderInstance addAction(string $hook, callable $callback, int $priority = 10, int $arguments = 1) { @see LoaderInstance::addAction() }
 * @method static LoaderInstance addFilter(string $hook, callable $callback, int $priority = 10, int $arguments = 1) { @see LoaderInstance::addFilter() }
 * @method static LoaderInstance addScript(string $handle, string $path, array $dependencies = [], array $config = []) { @see LoaderInstance::addScript() }
 * @method static LoaderInstance addScriptData(string $handle, array $data = [], string $key = null) { @see LoaderInstance::addScriptData() }
 * @method static LoaderInstance addStyle(string $handle, string $path, array $dependencies = [], array $config = []) { @see LoaderInstance::addStyle() }
 * @method static LoaderInstance addAdminScript(string $handle, string $path, array $dependencies = [], array $config = []) { @see LoaderInstance::addAdminScript() }
 * @method static LoaderInstance addAdminScriptData(string $handle, array $data = [], string $key = null) { @see LoaderInstance::addAdminScriptData() }
 * @method static LoaderInstance addAdminStyle(string $handle, string $path, array $dependencies = [], array $config = []) { @see LoaderInstance::addAdminStyle() }
 * @method static LoaderInstance addBlockScript(string $handle, string $path, array $dependencies = [], array $config = []) { @see LoaderInstance::addBlockScript() }
 * @method static LoaderInstance addBlockScriptData(string $handle, array $data = [], string $key = null) { @see LoaderInstance::addBlockScriptData() }
 * @method static LoaderInstance addBlockStyle(string $handle, string $path, array $dependencies = [], array $config = []) { @see LoaderInstance::addBlockStyle() }
 * @method static void load() { @see LoaderInstance::load() }
 *
 * @package Threefold\WordPress\Core
 */
class Loader implements StaticLoaderInterface
{
    use LinkedStaticAccessorTrait;

    /**
     * @inheritDoc
     */
    public static function instance(...$args) : LoaderInterface
    {
        return static::resolveInstance($args);
    }

    /**
     * @inheritDoc
     */
    protected static function makeInstance($args = []) : LoaderInterface
    {
        return new LoaderInstance();
    }

}
