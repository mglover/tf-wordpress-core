<?php

namespace Threefold\WordPress\Core\Block;

use Exception;
use ReflectionClass;
use ReflectionException;
use Threefold\WordPress\Core\Loader\LoaderInterface;
use Threefold\WordPress\Core\View\ViewInterface;
use Threefold\WordPress\Core\StaticAccessor\StaticAccessorTrait;
use Threefold\WordPress\Core\AdminNotice;

/**
 * Class AbstractBlock
 *
 * @method static string blockNamespace() { @see AbstractBlock::_blockNamespace() }
 * @method static string category() { @see AbstractBlock::_category() }
 * @method static string categoryName() { @ee AbstractBlock::_categoryName() }
 * @method static string render(array $attributes = [], string $content = '') { @see AbstractBlock::_render() }
 *
 * @package Threefold\WordPress\Core\Block
 */
abstract class AbstractBlock
{
    use StaticAccessorTrait;

    /** @var string PREFIX used to uniquely namespace sets of blocks */
    const PREFIX = 'threefold';
    /** @var string|null BLOCK slug for the block */
    const BLOCK = null;
    /** @var string|null CATEGORY slug to be used to group blocks in the editor */
    const CATEGORY = null;
    /** @var string[] ALLOWED_FILTERS array of filter the block is allowed, used to unregister blocks on editor pages not allowed example post type specific editors */
    const ALLOWED_ON_CONTENT = ['all'];

    /** @var LoaderInterface */
    protected LoaderInterface $loader;
    /** @var ViewInterface */
    protected ViewInterface $view;
    /** @var array */
    protected array $attributes = [];

    /**
     * AbstractBlock constructor.
     *
     * @param LoaderInterface $loader
     * @param ViewInterface $view
     */
    public function __construct(LoaderInterface $loader, ViewInterface $view)
    {
        try {
            // Check required definitions are defined
            if (static::BLOCK === null) {
                throw new Exception(sprintf('The `%s` BLOCK is required', static::class));
            }

            $this->loader = $loader;
            $this->view = $view;

            // Register block
            $this->loader->addAction('init', [$this, 'register']);
        } catch (Exception $e) {
            AdminNotice::error($e->getMessage());
        }
    }

    /**
     * Register block
     *
     * @throws ReflectionException
     */
    public function register() : void
    {
        $namespace = $this->_blockNamespace();
        $scriptSlug = static::PREFIX . '-' . static::BLOCK;

        $config = [
            'editor_script' => $scriptSlug,
            'attributes' => array_map(fn($attribute) => array_merge(['default' => ''], $attribute), $this->attributes),
        ];

        // Register block editor script for backend.
        $this->loader->addBlockScript(
            $scriptSlug,
            sprintf('public/js/block-%s.js', static::BLOCK),
            $this->_jsDependencies(),
            [
                'dataKey' => sprintf('block-%s-config', static::BLOCK),
                'data' => [
                    'namespace' => $this->_blockNamespace(),
                    'category' => $this->_category(),
                ],
            ]
        );

        // Register block styles for both frontend + backend if file exists
        $publicPath = sprintf('public/css/block-%s-style.css', static::BLOCK);
        if ($this->view->getPath($publicPath)) {
            $styleSlug = $scriptSlug . '-style';
            $this->loader->addBlockStyle(
                $styleSlug,
                $publicPath,
                $this->_styleDependencies(),
                ['isAdmin' => false]
            );
            $config['style'] = $styleSlug;
        }

        // Register block editor styles for backend if file exists
        $publicPath = sprintf('public/css/block-%s-editor.css', static::BLOCK);
        if ($this->view->getPath($publicPath)) {
            $editorStyleSlug = $scriptSlug . '-editor-style';
            $this->loader->addBlockStyle(
                $editorStyleSlug,
                $publicPath,
                $this->_editorStyleDependencies()
            );
            $config['editor_style'] = $editorStyleSlug;
        }

        // Check template path exists then set render callback
        if ($this->getTemplatePath()) {
            $config['render_callback'] = [$this, '_render'];
        }

        register_block_type($namespace, $config);
    }

    /**
     * Return block registered namespace string
     *
     * @return string
     */
    public function _blockNamespace() : string
    {
        return static::PREFIX . '/' .static::BLOCK;
    }

    /**
     * Return block category string
     *
     * @return string
     */
    public function _category() : string
    {
        $category = 'threefold-block';
        // Check if block category defined
        if (!empty(static::CATEGORY)) {
            $category .= '-' . static::CATEGORY;
        }

        return $category;
    }

    /**
     * @return array
     */
    public function _styleDependencies() : array
    {
        return [];
    }

    /**
     * @return string[]
     */
    public function _editorStyleDependencies() : array
    {
        return ['wp-edit-blocks'];
    }

    /**
     * @return array
     */
    public function _jsDependencies() : array
    {
        return ['wp-blocks', 'wp-polyfill', 'wp-i18n', 'wp-block-editor', 'wp-components', 'wp-data'];
    }

    /**
     * Return block category display name string
     *
     * @return string
     */
    public function _categoryName() : string
    {
        $categoryName = 'Threefold Block';
        // Check block category defined
        if (!empty(static::CATEGORY)) {
            $categoryName = sprintf(
                '%s - %s',
                str_replace('-', ' ', ucwords(static::CATEGORY, '-')),
                $categoryName
            );
        }

        return $categoryName;
    }

    /**
     * Return block render from either template or content
     *
     * @param array $attributes
     * @param string $content
     *
     * @return string
     * @throws ReflectionException
     */
    public function _render(array $attributes = [], string $content = '') : string
    {
        // Get template path
        $templatePath = $this->getTemplatePath();
        // Check template path returned
        if ($templatePath) {
            // Use View helper to render the block
            $content = $this->view->render(
                $templatePath,
                [
                    'attributes' => $this->getRenderAttributes($attributes),
                    'content' => $content,
                ],
                false
            );
        }
        // Return rendered content
        return $content;
    }

    /**
     * Return attributes with default value if not defined
     *
     * @param array $attributes
     *
     * @return array
     */
    protected function getRenderAttributes(array $attributes = []) : array
    {
        // Generate block class name
        $blockClass = sprintf('wp-block-%s-%s', static::PREFIX, static::BLOCK);
        // Extract block classes
        $classNames = array_filter(explode(' ', trim($attributes['className'] ?? '')));
        // Add block class name if not found
        if (!in_array($blockClass, $classNames)) {
            array_unshift($classNames, $blockClass);
        }
        // Recombine block classes
        $attributes['className'] = join(' ', $classNames);
        // Assign default attributes if missing
        foreach ($this->attributes as $key => $config) {
            if (!isset($attributes[$key])) {
                $attributes[$key] = $config['default'] ?? '';
            }
        }
        // Return attributes
        return $attributes;
    }

    /**
     * Return template path if found
     *
     * @return string|null
     * @throws ReflectionException
     */
    protected function getTemplatePath() : ?string
    {
        // Use ReflectionClass to determine path to block
        // Without this the AbstractBlock path would be used not the extending block
        $reflector = new ReflectionClass($this);
        $templatePath = dirname($reflector->getFileName()) . '/template.php';
        // Use View helper to check template path exists
        return $this->view->getPath($templatePath);
    }

}
