# Threefold Core

### Install

To install this plugin it is recommended to do with [bedrock](https://roots.io/bedrock) and a composer install.

Once you have an instance running in your `composer.json` you will need to add a new vsc definition under `"repositories"`

```
{
  ...
  "repositories": [
    ...
    {
      "type": "vcs",
      "url": "https://code.14west.us/scm/threefold/wordpress-core.git"
    }
  ]
  ...
}
``` 

Now you can run `composer require threefold/wordpress-core` and the plugin will install to the `vendor` directory

## Release Notes

### 0.2.4
##### 2021/11/11
#### Fixes
* Removed monolog dependency

### 0.2.3
#### Fixes
* Fix error in AbstractPostType _getAll

### 0.2.2
#### Fixes
* Fix return type of _getAll in AbstractPostType when no results
* Fix return type of _getAll, _get, _getBySlug and _getByPost in AbstractTaxonomy when no results

### 0.2.1
#### Updates
* Moved AbstractPostType options page definition functions to OptionsPageTrait
* Added getAll function to AbstractPostType
* Added getFromPost function to AbstractTaxonomy
* Updated block dependencies definition to function calls

### 0.2.0
#### Updates
* Added `autoload` config to Loader for assets, defaults to true except for block scripts and styles
* Replaced Loader admin asset functions with config `isAdmin`, defaults to false except for block scripts and styles
* Updated Loader handling of merging script localise data
* Updated CarbonFields OptionsPage to store container as class property  
* Updated default block namespace from `threefold-block` to `threefold`
* Added additional block asset dependencies
* Updated Post and Taxonomy metadata traits locations and added interfaces
* Added `REWRITE` const to `AbstractPostType`
* Added `getAll`, `get` and `getBySlug` helper functions to `AbstractTaxonomy`
* Updated `Image` helper function names
* Updated OptionsPage traits and interfaces

#### Fixes
* Fixed Loader loading block admin/frontend assets correctly
* Fixed post type and taxonomy register hook priority
* Fixed block scripts always loading 
* Fix block localise data definition when registering blocks

### 0.1.3
#### Fixes
* Fix post type register hook priority

### 0.1.2
#### Updates
* Added AbstractPostType supports author by default

#### Fixes
* Fix cached svg image key definition  

### 0.1.1
#### Updates
* Allow View `renderPartial` to allow empty partial name and skip processing it

#### Fixes
* Fix AbstractPostType MetaData and OptionsPage definition order
* Fix AbstractBlock default attribute value definition
* Fix Loader enquing block scripts and styles
* Fix Loader throwing Exception on same script addition
* Fix View/ViewInterface mismatched definition

### 0.1.0 
* Initial code
